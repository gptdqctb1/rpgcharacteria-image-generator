import { Injectable } from '@nestjs/common';
import { Configuration, OpenAIApi } from 'openai';
import * as process from 'process';
import { characterData } from './open-ai.controller';

@Injectable()
export class OpenAiService {
  private readonly openai: OpenAIApi;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {
    const configuration = new Configuration({
      apiKey: process.env.OPENAI_API_KEY,
    });
    this.openai = new OpenAIApi(configuration);
  }

  async generateImage(body: characterData): Promise<string> {
    const prompt = this.generateImageDescription(body);
    try {
      const response = await this.openai.createImage({
        prompt: prompt,
        size: '512x512',
        response_format: 'url',
      });

      return response.data.data[0].url;
    } catch (e) {
      console.log('error =', e.response.data);
      throw new Error(e);
    }
  }

  generateImageDescription(body: characterData): string {
    // return `On est dans un jeu de rôle, invente moi un personnage réaliste en entier, de genre ${body.genre}, qui a pour race ${body.race}, de classe ${body.classe}, de métier ${body.metier}, qui marche dans la fôret`;
    // return `Thème jeu de rôle, genre ${body.genre}, race ${body.race}, classe ${body.classe},paysage environnement de travail ${body.metier}, qui est ${body.caracteristic}, digital art`;
    return `Génère moi un personnage issue d'un jeu de role, ce personnage de genre ${body.genre} est un ${body.race} ${body.classe}, ses compétences en ${body.metier} sont incroyables, dans son perilleux voyage, il est devenu ${body.caracteristic}, digital art`;
  }
}
