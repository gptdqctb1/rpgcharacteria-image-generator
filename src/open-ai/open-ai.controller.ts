import {
  Controller,
  Get,
  HttpStatus,
  Logger,
  Query,
  Req,
  Res,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { OpenAiService } from './open-ai.service';

export interface characterData {
  race: string;
  genre: string;
  classe: string;
  metier: string;
  caracteristic: string;
}
@Controller('api/character-image-api/v1/')
export class OpenAiController {
  constructor(private readonly openAiService: OpenAiService) {}

  private readonly logger = new Logger(OpenAiController.name);
  @ApiBearerAuth()
  @Get()
  generate(@Query() body: characterData, @Req() req, @Res() res) {
    this.openAiService
      .generateImage(body)
      .then((result) => {
        this.logger.log('GET : ' + result);
        return res.status(HttpStatus.OK).send({ image: result });
      })
      .catch((e) => {
        this.logger.log('Error : ' + e);
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(e);
      });
    // return res.status(HttpStatus.OK).send({
    //   image:
    //     'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.ks5Hx9ne5qgaxbA8pbmXuQHaFj%26pid%3DApi&f=1&ipt=3130d0f9f9fd0a9b2d5eaf346ceb51b4c63fd2ba83128e637a6f9c7fdde99236&ipo=images',
    // });
  }
}
