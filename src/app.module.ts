import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AdminMiddleware } from './middlewares/admin.middleware';
import { ConfigModule } from '@nestjs/config';
import { FirebaseAdminService } from './service/firebase-admin.service';
import { OpenAiModule } from './open-ai/open-ai.module';
import { OpenAiService } from './open-ai/open-ai.service';
import { OpenAiController } from './open-ai/open-ai.controller';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), OpenAiModule],
  controllers: [OpenAiController],
  providers: [FirebaseAdminService, OpenAiService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(AdminMiddleware).forRoutes('api/character-image-api/v1/');
  }
}
